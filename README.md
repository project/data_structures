## INTRODUCTION

The Data Structures module is a developer utility module. It provides two sets
of tools to developers:

- Drush generator commands for easily creating classes to replace associative
  and indexed arrays with type safe objects.
- Data structure classes that instantiate typed sequences using the most common
  base types.

## BACKGROUND

We conducted a performance evaluation using 1,000,000 instances of data
structures with two items, one integer and one string.  Averaging 300 iterations
of instantiating and executing with the given data structure clearly shows that
properly structured objects are faster and more memory efficient than
associative arrays.

| Sorting           | Runtime |  Delta |     Memory |  Delta |
|-------------------|---------|-------:|-----------:|-------:|
| Assoc. Array      | 4.92563 |        | 499.547 MB |        |
| Object (public)   | 4.59630 |  -6.7% | 211.928 MB | -57.6% |
| Object (readonly) | 4.15491 | -15.6% | 211.928 MB | -57.6% |
| Object (private)  | 6.33438 | +28.6% | 211.930 MB | -57.6% |

| Serialization     | Runtime |  Delta |      Memory |  Delta |
|-------------------|---------|-------:|------------:|-------:|
| Assoc. Array      | 1.07523 |        | 1032.741 MB |        |
| Object (public)   | 1.09756 |  +2.1% |  849.519 MB | -17.7% |
| Object (readonly) | 1.35624 | +27.0% |  934.409 MB |  -9.5% |
| Object (private)  | 1.09738 |  +2.1% |  856.520 MB | -17.7% |

Typed sequences need to use an indexed array as an internal data structure, so
there are not equivalent efficiencies, however they do enforce a reliable type.

## USAGE

Install as you would normally install a contributed Drupal module.
`composer require drupal/data_structures`
See: [https://www.drupal.org/node/895232](https://www.drupal.org/u/fathershawn) for further information.

### Drush Commands

* `drush gen data-structures:map`: Generates a typed key-value class with public
   or readonly properties
* `data-structures:typed-sequence`: Generates a typed sequence class with utility methods.

### Sequences

Classes that can be used in custom code to enforce and manage typed sequences.

* `CallableImmutableSequence`
* `FloatImmutableSequence`
* `IntImmutableSequence`
* `ObjectImmutableSequence`
* `StringImmutableSequence`

### Set

A `Set` class, which can be extended to provide typed data, that ensures data
elements added to an instance are unique and has methods for performing set
operations between instances.

## MAINTAINERS

Current maintainers for Drupal 10:

- Shawn Duncan (FatherShawn) - [https://www.drupal.org/u/fathershawn](https://www.drupal.org/u/fathershawn)

