<?php

namespace Drupal\data_structures\DataStructure;

/**
 * A data structure for sets.
 *
 * By definition, every element of a set is unique within the set.
 */
interface SetInterface extends \IteratorAggregate, \Countable, \JsonSerializable {

  /**
   * Returns set data as an array.
   *
   * @return array
   *   The set values.
   */
  public function toArray(): array;

  /**
   * Empties the set.
   */
  public function empty(): void;

  /**
   * Determines if the set is empty.
   *
   * @return bool
   *   True if the set is empty.
   */
  public function isEmpty(): bool;

  /**
   * Adds new values to the set if they are not already members.
   *
   * @param mixed ...$values
   *   A variable number of values to add to the set.
   */
  public function add(...$values): void;

  /**
   * Removes values from the set if they are members.
   *
   * @param mixed ...$values
   *   A variable number of values to remove from the set.
   */
  public function remove(mixed ...$values): void;

  /**
   * Does the set have the value as a member?.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   True if the value is in the set
   */
  public function has(mixed $value): bool;

  /**
   * Finds the union with another set.
   *
   * +----------+
   * |$this.....|
   * |.....+-----------+
   * |.....|....|......|
   * |.....|....|......|
   * +-----|----+......|
   *       |...........|
   *       |......$set |
   *       +-----------+
   *
   * Union combines the members of both sets.
   *
   * @param \Drupal\data_structures\DataStructure\SetInterface $set
   *   The set to use in $this ∪ $set.
   *
   * @return \Drupal\data_structures\DataStructure\SetInterface
   *   The intersection.
   */
  public function union(SetInterface $set): SetInterface;

  /**
   * Finds the intersection with another set.
   *
   * +----------+
   * |$this     |
   * |     +-----------+
   * |     |....|      |
   * |     |....|      |
   * +-----|----+      |
   *       |           |
   *       |      $set |
   *       +-----------+
   *
   * Intersection finds the items that are members of both sets.
   *
   * @param \Drupal\data_structures\DataStructure\SetInterface $set
   *   The set to use in $this ∩ $set.
   *
   * @return \Drupal\data_structures\DataStructure\SetInterface
   *   The intersection.
   */
  public function intersect(SetInterface $set): SetInterface;

  /**
   * Finds the difference with another set.
   *
   * +----------+
   * |$this.....|
   * |.....+-----------+
   * |.....|    |      |
   * |.....|    |      |
   * +-----|----+      |
   *       |           |
   *       |      $set |
   *       +-----------+
   *
   * Difference finds the items that are members of the first set but not the
   * second.
   *
   * @param \Drupal\data_structures\DataStructure\SetInterface $set
   *   The set to use in $this \ $set.
   *
   * @return \Drupal\data_structures\DataStructure\SetInterface
   *   The intersection.
   */
  public function difference(SetInterface $set): SetInterface;

  /**
   * Finds the symmetric difference with another set.
   *
   * +----------+
   * |$this.....|
   * |.....+-----------+
   * |.....|    |......|
   * |.....|    |......|
   * +-----|----+......|
   *       |...........|
   *       |......$set |
   *       +-----------+
   *
   * Symmetric difference finds the items that are members of both sets that
   * are not also members of both sets.
   *
   * @param \Drupal\data_structures\DataStructure\SetInterface $set
   *   The set to use in $this \ $set.
   *
   * @return \Drupal\data_structures\DataStructure\SetInterface
   *   The intersection.
   */
  public function symDifference(SetInterface $set): SetInterface;

  /**
   * Applies a callable to re-map the set.
   *
   * @param callable $callable
   *   A callable that accepts a Set member and returns a new value.
   *
   * @return \Drupal\data_structures\DataStructure\SetInterface
   *   The result of the mapping.
   */
  public function map(callable $callable): SetInterface;

  /**
   * Applies a callable to reduce the set to a single value.
   *
   * @param callable $callable
   *   The callable signature is: callback(mixed $carry, mixed $item): mixed.
   * @param mixed $initial
   *   The initial value to pass as the "carry" value to the callable.
   *
   * @return mixed
   *   Returns the last return value from the callback.
   */
  public function reduce(callable $callable, mixed $initial = NULL): mixed;

  /**
   * Applies a callable to filter the set into a new set.
   *
   * @param callable $callable
   *   The callable signature is: callback(mixed $item): bool.
   *
   * @return \Drupal\data_structures\DataStructure\SetInterface
   *   The filtered sequence..
   */
  public function filter(callable $callable): SetInterface;

}
