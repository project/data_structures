<?php

declare(strict_types=1);

namespace Drupal\data_structures\DataStructure;

/**
 * An immutable sequence enforced to be of type callable.
 */
class CallableImmutableSequence implements \IteratorAggregate, \Countable, \ArrayAccess, \JsonSerializable {

  /**
   * Internal value storage.
   *
   * @var callable[]
   */
  protected array $values;

  /**
   * Constructor.
   *
   * @param iterable $values
   *   An iterable data set of values.
   */
  final public function __construct(iterable $values) {
    $this->values = [];
    foreach ($values as $value) {
      if (!(is_callable($value))) {
        throw new \TypeError('Values of ' . self::class . ' must be type callable');
      }
      $this->values[] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    foreach ($this->values as $value) {
      yield $value;
    }
  }

  /**
   * Returns sequence data as an array.
   *
   * @return callable[]
   *   The sequence values as an array.
   */
  public function toArray(): array {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists(mixed $offset): bool {
    return isset($this->values[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet(mixed $offset): callable {
    return $this->values[$offset];
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet(mixed $offset, mixed $value): void {
    throw new \RuntimeException('Instances of ' . self::class . ' are immutable.');
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset(mixed $offset): void {
    throw new \RuntimeException('Instances of ' . self::class . ' are immutable.');
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->values);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Is the sequence empty of values?
   *
   * @return bool
   *   True if empty.
   */
  public function isEmpty(): bool {
    return empty($this->values);
  }

  /**
   * Applies a callable to re-map the sequence.
   *
   * @param callable $callable
   *   A callable that accepts a string value and returns a string value.
   *
   * @return CallableImmutableSequence
   *   The result of the mapping.
   */
  public function map(callable $callable): CallableImmutableSequence {
    return new static(array_map($callable, $this->values));
  }

  /**
   * Applies a callable to reduce the sequence to a single value.
   *
   * @param callable $callable
   *   The callable signature is: callback(mixed $carry, string $item): mixed.
   * @param mixed $initial
   *   The initial value to pass as the "carry" value to the callable.
   *
   * @return mixed
   *   Returns the last return value from the callback.
   */
  public function reduce(callable $callable, mixed $initial = NULL): mixed {
    return array_reduce($this->values, $callable, $initial);
  }

  /**
   * Applies a callable to filter the sequence to a new sequence.
   *
   * @param callable $callable
   *   The callable signature is: callback(string $item): bool.
   *
   * @return CallableImmutableSequence
   *   The filtered sequence..
   */
  public function filter(callable $callable): CallableImmutableSequence {
    return new static(array_filter($this->values, $callable));
  }

}
