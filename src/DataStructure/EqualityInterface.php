<?php

namespace Drupal\data_structures\DataStructure;

/**
 * An interface that defines a broader equality between objects.
 *
 * PHP Objects use a default comparison strategy for equality:
 * https://www.php.net/manual/en/language.oop5.object-comparison.php
 * This interface allows objects to define their own rules for equality.
 *
 * @see https://github.com/php-ds/polyfill/blob/master/src/Hashable.php
 */
interface EqualityInterface {

  /**
   * Calculate a scalar value which can be used to compare to instances.
   *
   * Used to compare two instances of EqualityInterface.
   *
   * @return int|float|string
   *   The value for comparison.
   */
  public function hash(): int|float|string;

  /**
   * Comparison function to determine equality.
   *
   * The PHP type system cannot enforce it via the interface, but the
   * implementation should include checking that $instance is of the same
   * class as the class implementing this method.
   *
   * @param \Drupal\data_structures\DataStructure\EqualityInterface $instance
   *   An instance to compare to this object.
   *
   * @return bool
   *   True if $instance should be considered equal to $this.
   */
  public function equalTo(EqualityInterface $instance): bool;

}
