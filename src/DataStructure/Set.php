<?php

declare(strict_types=1);

namespace Drupal\data_structures\DataStructure;

/**
 * A general implementation for Set.
 */
class Set implements SetInterface {

  /**
   * Internal storage for set members.
   *
   * @var array
   */
  protected array $members = [];

  /**
   * Optionally add values at Set construction.
   *
   * @param iterable|null $values
   *   Contains a variable number of values to add to the set.
   */
  final public function __construct(?iterable $values) {
    if (!is_null($values)) {
      $this->add(...$values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return $this->members;
  }

  /* ***** Interface Methods **** */

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->members);
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->members);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->members;
  }

  /* ***** Maintaining the Set **** */

  /**
   * {@inheritdoc}
   */
  public function empty(): void {
    $this->members = [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return empty($this->members);
  }

  /**
   * {@inheritdoc}
   */
  public function add(...$values): void {
    foreach ($values as $value) {
      if (!$this->has($value)) {
        $this->members[] = $value;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function remove(mixed ...$values): void {
    $filterSet = new static(NULL);
    $filterSet->add(...$values);
    // $filter finds values not in the filter set.
    $filter = fn($value) => !$filterSet->has($value);
    $filtered = array_filter($this->members, $filter);
    $this->members = $filtered;
  }

  /**
   * {@inheritdoc}
   */
  public function has(mixed $value):bool {
    $found = FALSE;
    foreach ($this->members as $member) {
      if ($this->membersAreEqual($member, $value)) {
        $found = TRUE;
        break;
      }
    }
    return $found;
  }

  /**
   * {@inheritdoc}
   */
  public function union(SetInterface $set): Set {
    $union = new \AppendIterator();
    $union->append(new \IteratorIterator($this));
    $union->append(new \IteratorIterator($set));
    return new static($union);
  }

  /**
   * {@inheritdoc}
   */
  public function intersect(SetInterface $set): Set {
    $filter = fn($value) => $set->has($value);
    $intersection = array_filter($this->members, $filter);
    return new static($intersection);
  }

  /**
   * {@inheritdoc}
   */
  public function difference(SetInterface $set): Set {
    $filter = fn($value) => !$set->has($value);
    $intersection = array_filter($this->members, $filter);
    return new static($intersection);
  }

  /**
   * {@inheritdoc}
   */
  public function symDifference(SetInterface $set): Set {
    return $this->difference($set)->union($set->difference($this));
  }

  /**
   * {@inheritdoc}
   */
  public function map(callable $callable): Set {
    return new static(array_map($callable, $this->members));
  }

  /**
   * {@inheritdoc}
   */
  public function reduce(callable $callable, mixed $initial = NULL): mixed {
    return array_reduce($this->members, $callable, $initial);
  }

  /**
   * {@inheritdoc}
   */
  public function filter(callable $callable): Set {
    return new static(array_filter($this->members, $callable));
  }

  /* ***** Internal Methods **** */

  /**
   * Comparison function to determine equality.
   *
   * If both members implement EqualityInterface, then they define equality
   * for themselves.  Otherwise the === operator will be used to determine
   * equality.
   *
   * @param mixed $one
   *   An existing or possible member of the set.
   * @param mixed $two
   *   An existing or possible member of the set.
   *
   * @return bool
   *   True if $one and $two are equal.
   */
  protected function membersAreEqual(mixed $one, mixed $two): bool {
    if ($one instanceof EqualityInterface && $two instanceof EqualityInterface) {
      return get_class($one) === get_class($two) && $one->equalTo($two);
    }
    return $one === $two;
  }

}
