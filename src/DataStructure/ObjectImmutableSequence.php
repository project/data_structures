<?php

declare(strict_types=1);

namespace Drupal\data_structures\DataStructure;

/**
 * An immutable sequence enforced to be of type object.
 */
class ObjectImmutableSequence implements \IteratorAggregate, \Countable, \ArrayAccess, \JsonSerializable {

  /**
   * Internal value storage.
   *
   * @var object[]
   */
  protected array $values;

  /**
   * Constructor.
   *
   * @param iterable $values
   *   An iterable data set of values.
   * @param bool|callable $sort
   *   Optional: callable with signature as: callback(string $a, string $b): int
   *   or TRUE will sort using sort().  Defaults to FALSE: unsorted.
   */
  final public function __construct(iterable $values, bool|callable $sort = FALSE) {
    $this->values = [];
    foreach ($values as $value) {
      if (!(is_object($value))) {
        throw new \TypeError('Values of ' . self::class . ' must be type object');
      }
      $this->values[] = $value;
    }
    if (is_callable($sort)) {
      uasort($this->values, $sort);
      $this->values = array_values($this->values);
    }
    elseif ($sort) {
      sort($this->values);
      $this->values = array_values($this->values);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    foreach ($this->values as $value) {
      yield $value;
    }
  }

  /**
   * Returns sequence data as an array.
   *
   * @return object[]
   *   The sequence values as an array.
   */
  public function toArray(): array {
    return $this->values;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists(mixed $offset): bool {
    return isset($this->values[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet(mixed $offset): object {
    return $this->values[$offset];
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet(mixed $offset, mixed $value): void {
    throw new \RuntimeException('Instances of ' . self::class . ' are immutable.');
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset(mixed $offset): void {
    throw new \RuntimeException('Instances of ' . self::class . ' are immutable.');
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->values);
  }

  /**
   * {@inheritdoc}
   */
  public function jsonSerialize(): array {
    return $this->toArray();
  }

  /**
   * Is the sequence empty of values?
   *
   * @return bool
   *   True if empty.
   */
  public function isEmpty(): bool {
    return empty($this->values);
  }

  /**
   * Applies a callable to re-map the sequence.
   *
   * @param callable $callable
   *   A callable that accepts a string value and returns a string value.
   *
   * @return ObjectImmutableSequence
   *   The result of the mapping.
   */
  public function map(callable $callable): ObjectImmutableSequence {
    return new static(array_map($callable, $this->values));
  }

  /**
   * Applies a callable to reduce the sequence to a single value.
   *
   * @param callable $callable
   *   The callable signature is: callback(mixed $carry, string $item): mixed.
   * @param mixed $initial
   *   The initial value to pass as the "carry" value to the callable.
   *
   * @return mixed
   *   Returns the last return value from the callback.
   */
  public function reduce(callable $callable, mixed $initial = NULL): mixed {
    return array_reduce($this->values, $callable, $initial);
  }

  /**
   * Applies a callable to filter the sequence to a new sequence.
   *
   * @param callable $callable
   *   The callable signature is: callback(string $item): bool.
   *
   * @return StringImmutableSequence
   *   The filtered sequence..
   */
  public function filter(callable $callable): ObjectImmutableSequence {
    return new static(array_filter($this->values, $callable));
  }

}
