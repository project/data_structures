<?php

namespace Drupal\data_structures\Drush\Generators;

use Drupal\data_structures\BaseTypes;
use Drupal\data_structures\ClassTypes;
use Drupal\data_structures\DataStructure\StringImmutableSequence;
use Drush\Drush;

/**
 * A re-usable method to build a sequence of valid types.
 */
trait ClassCompleter {

  /**
   * Filters the list of valid types using 'contains' logic.
   *
   * @return \Drupal\data_structures\DataStructure\StringImmutableSequence
   *   A sequence of valid types.
   */
  public function getValidTypes(): StringImmutableSequence {
    if ($this->validTypes->isEmpty()) {
      $baseTypes = [];
      foreach (BaseTypes::cases() as $baseType) {
        $baseTypes[] = $baseType->value;
      }
      $classFinder = new ClassTypes(Drush::bootstrapManager()->drupalFinder());
      $this->validTypes = new StringImmutableSequence(array_merge($baseTypes, $classFinder->toArray()));
    }
    return $this->validTypes;
  }

}
