<?php

declare(strict_types=1);

namespace Drupal\data_structures\Drush\Generators;

use Drupal\data_structures\BaseTypes;
use Drupal\data_structures\DataStructure\StringImmutableSequence;
use Drupal\data_structures\Validator\PropertyName;
use Drupal\data_structures\Validator\Type;
use DrupalCodeGenerator\Asset\AssetCollection as Assets;
use DrupalCodeGenerator\Attribute\Generator;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\GeneratorType;
use DrupalCodeGenerator\Validator\Chained;
use DrupalCodeGenerator\Validator\Required;
use Drush\Attributes as CLI;
use Drush\Boot\DrupalBootLevels;
use Symfony\Component\Console\Question\Question;

/**
 * Generator for a typed key-value class with public properties.
 */
#[Generator(
  name: 'data-structures:map',
  description: 'Generates a typed key-value class with public or readonly properties.',
  templatePath: __DIR__ . '/templates',
  type: GeneratorType::MODULE_COMPONENT,
)]
final class MapGenerator extends BaseGenerator {

  use ClassCompleter;

  /**
   * Base types and available class names.
   *
   * @var \Drupal\data_structures\DataStructure\StringImmutableSequence
   */
  private StringImmutableSequence $validTypes;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct();
    $this->validTypes = new StringImmutableSequence([]);
  }

  /**
   * {@inheritdoc}
   */
  #[CLI\Bootstrap(level: DrupalBootLevels::FULL)]
  protected function generate(array &$vars, Assets $assets): void {
    $ir = $this->createInterviewer($vars);
    $vars['machine_name'] = $ir->askMachineName();
    $vars['class'] = $ir->askClass(default: '{machine_name|camelize}ValueMap');
    $vars['mutable'] = $ir->confirm('Are map values allowed to change after construction?');
    $vars['properties'] = [];
    while (!count($vars['properties']) || $ir->confirm('Add another typed property?')) {
      $property = $ir->ask('Property name', 'lowerCamelCase', new Chained(new Required(), new PropertyName()));
      $classQuestion = new Question('Base type or Namespaced Class');
      $classQuestion->setValidator(new Chained(new Required(), new Type()));
      $classQuestion->setAutocompleterValues($this->getValidTypes());
      $type = $this->io()->askQuestion($classQuestion);
      $base_type = BaseTypes::tryFrom($type);
      $isClass = !($base_type instanceof BaseTypes);
      $parsedType = explode('\\', $type);
      $shortType = array_pop($parsedType);
      $vars['properties'][] = [
        'full' => $isClass ? $type : '',
        'type' => $shortType,
        'name' => $property,
      ];
    }
    if ($vars['mutable']) {
      $assets->addFile('src/DataStructure/{class}.php', 'map-mutable.php.twig');
    }
    else {
      $assets->addFile('src/DataStructure/{class}.php', 'map-immutable.php.twig');
    }
  }

}
