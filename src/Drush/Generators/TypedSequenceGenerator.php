<?php

namespace Drupal\data_structures\Drush\Generators;

use Drupal\data_structures\BaseTypes;
use Drupal\data_structures\DataStructure\StringImmutableSequence;
use Drupal\data_structures\Validator\Type;
use DrupalCodeGenerator\Asset\AssetCollection;
use DrupalCodeGenerator\Attribute\Generator;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\GeneratorType;
use DrupalCodeGenerator\Validator\Chained;
use DrupalCodeGenerator\Validator\Required;
use Symfony\Component\Console\Question\Question;

/**
 * Generator for a typed key-value class with public properties.
 */
#[Generator(
  name: 'data-structures:typed-sequence',
  description: 'Generates a typed sequence class with utility methods.',
  templatePath: __DIR__ . '/templates',
  type: GeneratorType::MODULE_COMPONENT,
)]
class TypedSequenceGenerator extends BaseGenerator {

  use ClassCompleter;

  /**
   * Base types and available class names.
   *
   * @var \Drupal\data_structures\DataStructure\StringImmutableSequence
   */
  private StringImmutableSequence $validTypes;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct();
    $this->validTypes = new StringImmutableSequence([]);
  }

  /**
   * {@inheritdoc}
   */
  protected function generate(array &$vars, AssetCollection $assets): void {
    $ir = $this->createInterviewer($vars);
    $vars['machine_name'] = $ir->askMachineName();
    $classQuestion = new Question('Base type or Namespaced Class of sequence items');
    $classQuestion->setValidator(new Chained(new Required(), new Type()));
    $classQuestion->setAutocompleterValues($this->getValidTypes());
    $type = $this->io()->askQuestion($classQuestion);
    $base_type = BaseTypes::tryFrom($type);
    $isClass = !($base_type instanceof BaseTypes);
    $parsedType = explode('\\', $type);
    $vars['type'] = array_pop($parsedType);
    $vars['full'] = $isClass ? $type : '';
    $vars['class'] = $ir->askClass(default: '{type|camelize}ImmutableSequence');
    $assets->addFile('src/DataStructure/{class}.php', 'typed-sequence.php.twig');
  }

}
