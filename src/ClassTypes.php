<?php

namespace Drupal\data_structures;

use Drush\DrupalFinder\DrushDrupalFinder;
use Symfony\Component\Finder\Finder;

/**
 * Finds full class names from PHP files containing class declarations.
 */
class ClassTypes implements \IteratorAggregate, \Countable {

  /**
   * Fully qualified class name strings.
   *
   * @var string[]
   */
  protected array $classes = [];

  /**
   * Instantiate with DrupalFinder injected.
   *
   * @param \Drush\DrupalFinder\DrushDrupalFinder $drupalFinder
   *   Injected finder.
   */
  public function __construct(DrushDrupalFinder $drupalFinder) {
    $finder = Finder::create()
      ->append(
        Finder::create()
          ->in($drupalFinder->getDrupalRoot())
          ->files()
          ->name('*.php')
          ->contains('/namespace .*;/')
          ->contains('/class [A-Z][a-zA-Z0-9]+/')
      )
      ->append(
        Finder::create()
          ->in($drupalFinder->getVendorDir())
          ->files()
          ->name('*.php')
          ->contains('/namespace .*;/')
          ->contains('/class [A-Z][a-zA-Z0-9]+/')
      );

    foreach ($finder as $fileInfo) {
      $detectedNamespace = $detectedClass = [];
      $contents = $fileInfo->getContents();
      preg_match('/namespace (.*);/', $contents, $detectedNamespace);
      $match = array_pop($detectedNamespace);
      $namespace = $match ?? '';
      preg_match('/class ([A-Z][a-zA-Z0-9]+)/', $contents, $detectedClass);
      $match = array_pop($detectedClass);
      $class = $match ?? '';
      $this->classes[] = $namespace . '\\' . $class;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->classes);
  }

  /**
   * {@inheritdoc}
   */
  public function count(): int {
    return count($this->classes);
  }

  /**
   * Returns class data as an array.
   *
   * @return string[]
   *   The class names.
   */
  public function toArray(): array {
    return $this->classes;
  }

}
