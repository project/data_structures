<?php

namespace Drupal\data_structures\Validator;

/**
 * A validator for strings used as property names.
 */
final class PropertyName {

  /**
   * Check for proper property name value.
   *
   * @throws \UnexpectedValueException
   */
  public function __invoke(mixed $value): string {
    if (!\is_string($value) || !\preg_match('/^[a-z]+((\d)|([A-Z0-9][a-z0-9]+))*([A-Z])?$/', $value)) {
      throw new \UnexpectedValueException('The value is not a correct property name.');
    }
    return $value;
  }

}
