<?php

namespace Drupal\data_structures\Validator;

use Drupal\data_structures\BaseTypes;

/**
 * A validator for strings used as types.
 */
final class Type {

  /**
   * Check for valid namespaced type identifier value.
   *
   * @throws \UnexpectedValueException
   */
  public function __invoke(mixed $value): string {
    $base_type = BaseTypes::tryFrom($value);
    $valid_class = is_string($value);
    $parts = explode('\\', $value);
    foreach ($parts as $part) {
      $valid_class = $valid_class && preg_match('/^([A-Z][a-z0-9]+)((\d)|([A-Z0-9][a-z0-9]+))*([A-Z])?$/', $part) === 1;
    }
    if (!\is_string($value) || (!($base_type instanceof BaseTypes) && !$valid_class)) {
      throw new \UnexpectedValueException('The value is not a correct type value.');
    }
    return $value;
  }

}
