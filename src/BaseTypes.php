<?php

namespace Drupal\data_structures;

/**
 * Enum for supported base types.
 *
 * @internal
 */
enum BaseTypes: string {
  case Bool = 'bool';
  case IntType = 'int';
  case FloatType = 'float';
  case StringType = 'string';
  case ArrayType = 'array';
  case ObjectType = 'object';
  case ResourceType = 'resource';
  case NeverType = 'never';
  case VoidType = 'void';
  case FalseType = 'false';
  case TrueType = 'true';
  case CallableType = 'callable';
}
