<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\DataStructure\CallableImmutableSequence;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 */
final class CallableSequenceTest extends UnitTestCase {
  /**
   * Test values.
   *
   * @var callable[]
   */
  protected array $values = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $integers = [5, 9, 16, 18, 11, 1, 3];
    foreach ($integers as $value) {
      $function = fn($x) => $x + $value;
      $this->values[] = $function;
    }
  }

  /**
   * Tests the constructor with all 4 cases.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $simple = new CallableImmutableSequence($this->values);
    $this->assertInstanceOf(CallableImmutableSequence::class, $simple);
  }

  /**
   * Tests the sequence can be iterated with foreach.
   *
   * @covers ::getIterator, ::toArray
   */
  public function testIterator(): void {
    $simple = new CallableImmutableSequence($this->values);
    $iteratedValues = [];
    foreach ($simple as $value) {
      $iteratedValues[] = $value;
    }
    $this->assertSame($simple->toArray(), $iteratedValues);
  }

  /**
   * Tests the offset methods.
   *
   * @covers ::offsetExists, ::offsetGet, ::offsetSet, ::offsetUnset
   */
  public function testValidOffsets(): void {
    $simple = new CallableImmutableSequence($this->values);
    $this->assertTrue(isset($simple[0]));
    $test = $simple[0];
    $this->assertEquals(6, $test(1));
  }

  /**
   * Tests offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetSet(): void {
    $simple = new CallableImmutableSequence($this->values);

    $this->expectException('RuntimeException');
    $simple[1] = 'foo';
  }

  /**
   * Test offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetUnset(): void {
    $simple = new CallableImmutableSequence($this->values);

    $this->expectException('RuntimeException');
    unset($simple[0]);
  }

  /**
   * Tests remaining non-filter functions.
   *
   * @covers ::count, ::isEmpty, ::jsonSerialize
   */
  public function testCount(): void {
    $simple = new CallableImmutableSequence($this->values);

    $this->assertEquals(7, $simple->count());
    $this->assertFalse($simple->isEmpty());
    $this->assertSame($this->values, $simple->jsonSerialize());
  }

}
