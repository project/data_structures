<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\DataStructure\StringImmutableSequence;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 *
 * @coversDefaultClass \Drupal\data_structures\DataStructure\StringImmutableSequence
 */
final class StringSequenceTest extends UnitTestCase {

  /**
   * Test values.
   *
   * @var string[]
   */
  protected array $values = [
    'red',
    'orange',
    'yellow',
    'green',
    'blue',
    'indigo',
    'violet',
  ];

  /**
   * Test values sorted.
   *
   * @var string[]
   */
  protected array $sorted = [
    'blue',
    'green',
    'indigo',
    'orange',
    'red',
    'violet',
    'yellow',
  ];

  /**
   * Test values reversed.
   *
   * @var string[]
   */
  protected array $reversed = [
    'yellow',
    'violet',
    'red',
    'orange',
    'indigo',
    'green',
    'blue',
  ];

  /**
   * Tests the constructor with all 4 cases.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $simple = new StringImmutableSequence($this->values);
    $natSorted = new StringImmutableSequence($this->values, TRUE);
    $testSort = fn($a, $b) => (int) -1 * ($a <=> $b);
    $uaSorted = new StringImmutableSequence($this->values, $testSort);
    $this->assertInstanceOf(StringImmutableSequence::class, $simple);
    $this->assertInstanceOf(StringImmutableSequence::class, $natSorted);
    $this->assertInstanceOf(StringImmutableSequence::class, $uaSorted);
    $this->assertSame($natSorted->toArray(), $this->sorted);
    $this->assertSame($uaSorted->toArray(), $this->reversed);
  }

  /**
   * Tests the sequence can be iterated with foreach.
   *
   * @covers ::getIterator, ::toArray
   */
  public function testIterator(): void {
    $simple = new StringImmutableSequence($this->values);
    $iteratedValues = [];
    foreach ($simple as $value) {
      $iteratedValues[] = $value;
    }
    $this->assertSame($simple->toArray(), $iteratedValues);
  }

  /**
   * Tests the offset methods.
   *
   * @covers ::offsetExists, ::offsetGet, ::offsetSet, ::offsetUnset
   */
  public function testValidOffsets(): void {
    $simple = new StringImmutableSequence($this->values);
    $this->assertTrue(isset($simple[0]));
    $this->assertEquals('orange', $simple[1]);
  }

  /**
   * Tests offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetSet(): void {
    $simple = new StringImmutableSequence($this->values);
    $this->expectException('RuntimeException');
    $simple[1] = 'foo';
  }

  /**
   * Test offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetUnset(): void {
    $simple = new StringImmutableSequence($this->values);
    $this->expectException('RuntimeException');
    unset($simple[0]);
  }

  /**
   * Tests remaining non-filter functions.
   *
   * @covers ::count, ::isEmpty, ::jsonSerialize
   */
  public function testCount(): void {
    $simple = new StringImmutableSequence($this->values);
    $this->assertEquals(7, $simple->count());
    $this->assertFalse($simple->isEmpty());
    $this->assertSame($this->values, $simple->jsonSerialize());
  }

  /**
   * Tests contains.
   *
   * @covers ::filterContains
   */
  public function testFilterContains(): void {
    $simple = new StringImmutableSequence($this->values);
    $filtered = $simple->filterContains('dig');
    $this->assertEquals($filtered->count(), 1);
    $values = $filtered->toArray();
    self::assertEquals('indigo', reset($values));
  }

  /**
   * Tests begins.
   *
   * @covers ::filterBegins
   */
  public function testFilterBegins(): void {
    $simple = new StringImmutableSequence($this->values);
    $filtered = $simple->filterBegins('or');
    $this->assertEquals($filtered->count(), 1);
    $values = $filtered->toArray();
    self::assertEquals('orange', reset($values));
  }

  /**
   * Tests value mapping.
   *
   * @covers ::map
   */
  public function testMap(): void {
    $expected = [
      'red*',
      'orange*',
      'yellow*',
      'green*',
      'blue*',
      'indigo*',
      'violet*',
    ];
    $map = fn(string $value):string =>  $value .= '*';
    $simple = new StringImmutableSequence($this->values);
    $mapped = $simple->map($map);
    $this->assertSame($expected, $mapped->toArray());
  }

  /**
   * Tests value reduction.
   *
   * @covers ::reduce
   */
  public function testReduce(): void {
    $simple = new StringImmutableSequence($this->values);
    $reduction = fn(int $carry, string $item): int => $carry + strlen($item);
    $this->assertEquals(36, $simple->reduce($reduction, 0));
  }

  /**
   * Tests general filtering.
   *
   * @covers ::filter
   */
  public function testFilter(): void {
    $expected = [
      'red',
      'green',
      'blue',
    ];
    $filter = fn(string $value): bool => strlen($value) < 6;
    $simple = new StringImmutableSequence($this->values);
    $filtered = $simple->filter($filter);
    $this->assertEquals(3, $filtered->count());
    $this->assertSame($expected, $filtered->toArray());
  }

}
