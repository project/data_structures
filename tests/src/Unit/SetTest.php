<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\DataStructure\Set;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 * @coversDefaultClass \Drupal\data_structures\DataStructure\Set
 */
final class SetTest extends UnitTestCase {

  /**
   * Verify set creation.
   *
   * @covers ::__construct, ::add, ::has
   */
  public function testConstructor(): void {
    $memberValues = [1, 2, 3, 4];
    $set = new Set($memberValues);
    foreach ($memberValues as $value) {
      $this->assertTrue($set->has($value));
    }
    $this->assertFalse($set->has('foo'));
  }

  /**
   * Verify array export.
   *
   * @covers ::toArray, ::add
   */
  public function testArray(): void {
    $memberValues = [1, 2, 3, 4, 1];
    $set = new Set($memberValues);
    // Set should have only one entry for 1.
    $this->assertSame([1, 2, 3, 4], $set->toArray());
  }

  /**
   * Tests the sequence can be iterated with foreach.
   *
   * @covers ::getIterator, ::toArray
   */
  public function testIterator(): void {
    $memberValues = [1, 2, 3, 4];
    $set = new Set($memberValues);
    $iteratedValues = [];
    foreach ($set as $value) {
      $iteratedValues[] = $value;
    }
    $this->assertSame($set->toArray(), $iteratedValues);
  }

  /**
   * Verify accurate count.
   *
   * @covers ::count
   */
  public function testCount(): void {
    $memberValues = [1, 2, 3, 4, 1];
    $set = new Set($memberValues);
    // Set should have only one entry for 1.
    $this->assertEquals(4, $set->count());
  }

  /**
   * Verify format for JSON export.
   *
   * @covers ::jsonSerialize
   */
  public function testJson(): void {
    $memberValues = [1, 2, 3, 4, 1];
    $set = new Set($memberValues);
    // Set should have only one entry for 1.
    $this->assertSame([1, 2, 3, 4], $set->toArray());
  }

  /**
   * Verify checking for empty.
   *
   * @covers ::isEmpty, ::empty
   */
  public function testIsEmpty(): void {
    $set = new Set(NULL);
    $this->assertTrue($set->isEmpty());
    $set->add('foo');
    $this->assertEquals(1, $set->count());
    $set->empty();
    $this->assertTrue($set->isEmpty());
  }

  /**
   * Verify array export.
   *
   * @covers ::remove
   */
  public function testRemove(): void {
    $memberValues = [1, 2, 3, 4, 1];
    $set = new Set($memberValues);
    $set->remove(3, 4);
    $this->assertTrue($set->has(1));
    $this->assertTrue($set->has(2));
    $this->assertFalse($set->has(3));
    $this->assertFalse($set->has(4));
    $this->assertEquals(2, $set->count());
  }

  /**
   * Verify set unions.
   *
   * @covers ::union
   */
  public function testUnion(): void {
    $setA = new Set([1, 2, 3]);
    $setB = new Set([4, 5, 6]);
    $union = $setA->union($setB);
    $this->assertEquals(6, $union->count());
    foreach ($setA as $item) {
      $this->assertTrue($union->has($item));
    }
    foreach ($setB as $item) {
      $this->assertTrue($union->has($item));
    }
    $setB = new Set([2, 3, 4]);
    $union = $setA->union($setB);
    $this->assertEquals(4, $union->count());
  }

  /**
   * Verify set intersection.
   *
   * @covers ::intersect
   */
  public function testIntersection(): void {
    $setA = new Set([1, 2, 3]);
    $setB = new Set([2, 3, 4]);
    $intersect = $setB->intersect($setA);
    $this->assertEquals(2, $intersect->count());
    $this->assertTrue($intersect->has(2));
    $this->assertTrue($intersect->has(3));
  }

  /**
   * Verify set intersection.
   *
   * @covers ::intersect
   */
  public function testDifference(): void {
    $setA = new Set([1, 2, 3]);
    $setB = new Set([2, 3, 4]);
    $intersect = $setB->difference($setA);
    $this->assertEquals(1, $intersect->count());
    $this->assertFalse($intersect->has(2));
    $this->assertTrue($intersect->has(4));
  }

  /**
   * Verify set intersection.
   *
   * @covers ::intersect
   */
  public function testSymDifference(): void {
    $setA = new Set([1, 2, 3]);
    $setB = new Set([2, 3, 4]);
    $intersect = $setB->symDifference($setA);
    $this->assertEquals(2, $intersect->count());
    $this->assertFalse($intersect->has(2));
    $this->assertTrue($intersect->has(4));
    $this->assertTrue($intersect->has(1));
  }

  /**
   * Verify set mapping.
   *
   * @covers ::map
   */
  public function testMap(): void {
    $memberValues = [1, 2, 3, 4, 1];
    $expected = [2, 4, 6, 8];
    $set = new Set($memberValues);
    $mapping = fn(int $value): int => 2 * $value;
    $mapped = $set->map($mapping);
    $this->assertSame($mapped->toArray(), $expected);
  }

  /**
   * Verify set reduction.
   *
   * @covers ::reduce
   */
  public function testReduce(): void {
    $memberValues = [2, 4, 6, 8];
    $set = new Set($memberValues);
    $reduction = fn(int $carry, int $item): int => max($carry, $item);
    $this->assertEquals(8, $set->reduce($reduction, 0));
  }

  /**
   * Verify set filtering..
   *
   * @covers ::filter
   */
  public function testFilter(): void {
    $memberValues = [1, 2, 3, 4];
    $set = new Set($memberValues);
    $filter = fn(int $item): bool => $item % 2 === 0;
    $filtered = $set->filter($filter);
    $this->assertEquals(2, $filtered->count());
    $this->assertTrue($filtered->has(2));
    $this->assertTrue($filtered->has(4));
  }

}
