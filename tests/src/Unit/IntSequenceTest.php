<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\DataStructure\IntImmutableSequence;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 */
final class IntSequenceTest extends UnitTestCase {


  /**
   * Test values.
   *
   * @var int[]
   */
  protected array $values = [5, 9, 16, 18, 11, 1, 3];

  /**
   * Test values sorted.
   *
   * @var int[]
   */
  protected array $sorted = [1, 3, 5, 9, 11, 16, 18];

  /**
   * Test values reversed.
   *
   * @var int[]
   */
  protected array $reversed = [18, 16, 11, 9, 5, 3, 1];

  /**
   * Tests the constructor with all 4 cases.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $simple = new IntImmutableSequence($this->values);
    $sorted = new IntImmutableSequence($this->values, TRUE);
    $testSort = fn($a, $b) => (int) -1 * ($a <=> $b);
    $uaSorted = new IntImmutableSequence($this->values, $testSort);
    $this->assertInstanceOf(IntImmutableSequence::class, $simple);
    $this->assertInstanceOf(IntImmutableSequence::class, $sorted);
    $this->assertInstanceOf(IntImmutableSequence::class, $uaSorted);
    $this->assertSame($sorted->toArray(), $this->sorted);
    $this->assertSame($uaSorted->toArray(), $this->reversed);
  }

  /**
   * Tests the sequence can be iterated with foreach.
   *
   * @covers ::getIterator, ::toArray
   */
  public function testIterator(): void {
    $simple = new IntImmutableSequence($this->values);
    $iteratedValues = [];
    foreach ($simple as $value) {
      $iteratedValues[] = $value;
    }
    $this->assertSame($simple->toArray(), $iteratedValues);
  }

  /**
   * Tests the offset methods.
   *
   * @covers ::offsetExists, ::offsetGet, ::offsetSet, ::offsetUnset
   */
  public function testValidOffsets(): void {
    $simple = new IntImmutableSequence($this->values);
    $this->assertTrue(isset($simple[0]));
    $this->assertEquals(9, $simple[1]);
  }

  /**
   * Tests offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetSet(): void {
    $simple = new IntImmutableSequence($this->values);
    $this->expectException('RuntimeException');
    $simple[1] = 'foo';
  }

  /**
   * Test offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetUnset(): void {
    $simple = new IntImmutableSequence($this->values);
    $this->expectException('RuntimeException');
    unset($simple[0]);
  }

  /**
   * Tests remaining non-filter functions.
   *
   * @covers ::count, ::isEmpty, ::jsonSerialize
   */
  public function testCount(): void {
    $simple = new IntImmutableSequence($this->values);
    $this->assertEquals(7, $simple->count());
    $this->assertFalse($simple->isEmpty());
    $this->assertSame($this->values, $simple->jsonSerialize());
  }

  /**
   * Tests value mapping.
   *
   * @covers ::map
   */
  public function testMap(): void {
    $expected = [6, 10, 17, 19, 12, 2, 4];
    $map = fn(int $value):int =>  $value + 1;
    $simple = new IntImmutableSequence($this->values);
    $mapped = $simple->map($map);
    $this->assertSame($expected, $mapped->toArray());
  }

  /**
   * Tests value reduction.
   *
   * @covers ::reduce
   */
  public function testReduce(): void {
    $simple = new IntImmutableSequence($this->values);
    $reduction = fn(int $carry, int $item): int => $carry + $item;
    $this->assertEquals(63, $simple->reduce($reduction, 0));
  }

  /**
   * Tests general filtering.
   *
   * @covers ::filter
   */
  public function testFilter(): void {
    $expected = [5, 1, 3];
    $filter = fn(int $value): bool => $value < 6;
    $simple = new IntImmutableSequence($this->values);
    $filtered = $simple->filter($filter);
    $this->assertEquals(3, $filtered->count());
    $this->assertSame($expected, $filtered->toArray());
  }

}
