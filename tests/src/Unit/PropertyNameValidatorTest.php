<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\Validator\PropertyName;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 *
 * @coversDefaultClass \Drupal\data_structures\Validator\PropertyName
 */
final class PropertyNameValidatorTest extends UnitTestCase {

  /**
   * Tests lowercase property name.
   *
   * @covers ::__invoke
   */
  public function testLowerCase(): void {
    $validator = new PropertyName();
    $this->assertEquals('lowercase', $validator('lowercase'), 'Valid property names may be all lower case.');
  }

  /**
   * Tests lowercase property name.
   *
   * @covers ::__invoke
   */
  public function testLowerCamelCase(): void {
    $validator = new PropertyName();
    $this->assertEquals('lowerCamelCase', $validator('lowerCamelCase'), 'Valid property names may be all lowerCamelCase.');
  }

  /**
   * Tests full camel case.
   *
   * @covers ::__invoke
   */
  public function testFullCamelCase(): void {
    $this->expectException(\UnexpectedValueException::class);
    $validator = new PropertyName();
    $validator('FullCamelCase');
  }

  /**
   * Tests leading digits.
   *
   * @covers ::__invoke
   */
  public function testLeadingDigits(): void {
    $this->expectException(\UnexpectedValueException::class);
    $validator = new PropertyName();
    $validator('9FullCamelCase');
  }

}
