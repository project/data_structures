<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\DataStructure\ObjectImmutableSequence;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 */
final class ObjectSequenceTest extends UnitTestCase {

  /**
   * Test values.
   *
   * @var TestMap[]
   */
  protected array $values = [];

  /**
   * Test values sorted.
   *
   * @var TestMap[]
   */
  protected array $sorted = [];

  /**
   * Test values reversed.
   *
   * @var TestMap[]
   */
  protected array $reversed = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $integers = [5, 9, 16, 18, 11, 1, 3];
    foreach ($integers as $value) {
      $map = new TestMap();
      $map->a = $value;
      $map->b = 3 * $value;
      $this->values[] = $map;
      $this->sorted[] = $map;
    }
    sort($this->sorted);
    $this->sorted = array_values($this->sorted);
    $this->reversed = array_reverse($this->sorted);
  }

  /**
   * Tests the constructor with all 4 cases.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $simple = new ObjectImmutableSequence($this->values);
    $natSorted = new ObjectImmutableSequence($this->values, TRUE);
    $testSort = fn($a, $b) => (int) -1 * ($a <=> $b);
    $uaSorted = new ObjectImmutableSequence($this->values, $testSort);
    $this->assertInstanceOf(ObjectImmutableSequence::class, $simple);
    $this->assertInstanceOf(ObjectImmutableSequence::class, $natSorted);
    $this->assertInstanceOf(ObjectImmutableSequence::class, $uaSorted);
    $this->assertSame($natSorted->toArray(), $this->sorted);
    $this->assertSame($uaSorted->toArray(), $this->reversed);
  }

  /**
   * Tests the sequence can be iterated with foreach.
   *
   * @covers ::getIterator, ::toArray
   */
  public function testIterator(): void {
    $simple = new ObjectImmutableSequence($this->values);
    $iteratedValues = [];
    foreach ($simple as $value) {
      $iteratedValues[] = $value;
    }
    $this->assertSame($simple->toArray(), $iteratedValues);
  }

  /**
   * Tests the offset methods.
   *
   * @covers ::offsetExists, ::offsetGet, ::offsetSet, ::offsetUnset
   */
  public function testValidOffsets(): void {
    $simple = new ObjectImmutableSequence($this->values);
    $this->assertTrue(isset($simple[0]));
    $test = new TestMap();
    $test->a = 9;
    $test->b = 27;
    $this->assertEquals($test, $simple[1]);
  }

  /**
   * Tests offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetSet(): void {
    $simple = new ObjectImmutableSequence($this->values);

    $this->expectException('RuntimeException');
    $simple[1] = 'foo';
  }

  /**
   * Test offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetUnset(): void {
    $simple = new ObjectImmutableSequence($this->values);

    $this->expectException('RuntimeException');
    unset($simple[0]);
  }

  /**
   * Tests remaining non-filter functions.
   *
   * @covers ::count, ::isEmpty, ::jsonSerialize
   */
  public function testCount(): void {
    $simple = new ObjectImmutableSequence($this->values);

    $this->assertEquals(7, $simple->count());
    $this->assertFalse($simple->isEmpty());
    $this->assertSame($this->values, $simple->jsonSerialize());
  }

  /**
   * Tests value mapping.
   *
   * @covers ::map
   */
  public function testMap(): void {
    $expected = [
      [6, 15],
      [10, 27],
      [17, 48],
      [19, 54],
      [12, 33],
      [2, 3],
      [4, 9],
    ];
    $map = function (TestMap $value):TestMap {
      $value->a = $value->a + 1;
      return $value;
    };
    $simple = new ObjectImmutableSequence($this->values);

    $mapped = $simple->map($map);
    foreach ($mapped as $item) {
      $next = array_shift($expected);
      $this->assertSame($next, [$item->a, $item->b]);
    }
  }

  /**
   * Tests value reduction.
   *
   * @covers ::reduce
   */
  public function testReduce(): void {
    $simple = new ObjectImmutableSequence($this->values);

    $reduction = fn(int $carry, TestMap $item): int => $carry + $item->a;
    $this->assertEquals(63, $simple->reduce($reduction, 0));
  }

  /**
   * Tests general filtering.
   *
   * @covers ::filter
   */
  public function testFilter(): void {
    $filter = fn(TestMap $value): bool => $value->a < 6;
    $simple = new ObjectImmutableSequence($this->values);

    $filtered = $simple->filter($filter);
    $this->assertEquals(3, $filtered->count());
  }

}

/**
 * A testing class for Maps.
 */
final class TestMap {

  /**
   * The a value.
   *
   * @var int
   */
  public int $a;

  /**
   * The b value.
   *
   * @var int
   */
  public int $b;

}
