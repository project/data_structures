<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\DataStructure\FloatImmutableSequence;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 */
final class FloatSequenceTest extends UnitTestCase {

  /**
   * Test values.
   *
   * @var float[]
   */
  protected array $values = [0.5, 0.9, 1.6, 1.8, 1.1, 0.1, 0.3];

  /**
   * Test values sorted.
   *
   * @var string[]
   */
  protected array $sorted = [0.1, 0.3, 0.5, 0.9, 1.1, 1.6, 1.8];

  /**
   * Test values reversed.
   *
   * @var string[]
   */
  protected array $reversed = [1.8, 1.6, 1.1, 0.9, 0.5, 0.3, 0.1];

  /**
   * Tests the constructor with all 4 cases.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $simple = new FloatImmutableSequence($this->values);
    $sorted = new FloatImmutableSequence($this->values, TRUE);
    $testSort = fn($a, $b) => (int) -1 * ($a <=> $b);
    $uaSorted = new FloatImmutableSequence($this->values, $testSort);
    $this->assertInstanceOf(FloatImmutableSequence::class, $simple);
    $this->assertInstanceOf(FloatImmutableSequence::class, $sorted);
    $this->assertInstanceOf(FloatImmutableSequence::class, $uaSorted);
    $this->assertSame($sorted->toArray(), $this->sorted);
    $this->assertSame($uaSorted->toArray(), $this->reversed);
  }

  /**
   * Tests the sequence can be iterated with foreach.
   *
   * @covers ::getIterator, ::toArray
   */
  public function testIterator(): void {
    $simple = new FloatImmutableSequence($this->values);
    $iteratedValues = [];
    foreach ($simple as $value) {
      $iteratedValues[] = $value;
    }
    $this->assertSame($simple->toArray(), $iteratedValues);
  }

  /**
   * Tests the offset methods.
   *
   * @covers ::offsetExists, ::offsetGet, ::offsetSet, ::offsetUnset
   */
  public function testValidOffsets(): void {
    $simple = new FloatImmutableSequence($this->values);
    $this->assertTrue(isset($simple[0]));
    $this->assertEquals(0.9, $simple[1]);
  }

  /**
   * Tests offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetSet(): void {
    $simple = new FloatImmutableSequence($this->values);
    $this->expectException('RuntimeException');
    $simple[1] = 'foo';
  }

  /**
   * Test offsetSet.
   *
   * @covers ::offsetSet
   */
  public function testOffsetUnset(): void {
    $simple = new FloatImmutableSequence($this->values);
    $this->expectException('RuntimeException');
    unset($simple[0]);
  }

  /**
   * Tests remaining non-filter functions.
   *
   * @covers ::count, ::isEmpty, ::jsonSerialize
   */
  public function testCount(): void {
    $simple = new FloatImmutableSequence($this->values);
    $this->assertEquals(7, $simple->count());
    $this->assertFalse($simple->isEmpty());
    $this->assertSame($this->values, $simple->jsonSerialize());
  }

  /**
   * Tests value mapping.
   *
   * @covers ::map
   */
  public function testMap(): void {
    $expected = [1.5, 1.9, 2.6, 2.8, 2.1, 1.1, 1.3];
    $map = fn(float $value):float =>  $value + 1.0;
    $simple = new FloatImmutableSequence($this->values);
    $mapped = $simple->map($map);
    $this->assertSame($expected, $mapped->toArray());
  }

  /**
   * Tests value reduction.
   *
   * @covers ::reduce
   */
  public function testReduce(): void {
    $simple = new FloatImmutableSequence($this->values);
    $reduction = fn(float $carry, float $item): float => $carry + $item;
    $this->assertEquals(6.3, $simple->reduce($reduction, 0));
  }

  /**
   * Tests general filtering.
   *
   * @covers ::filter
   */
  public function testFilter(): void {
    $expected = [0.5, 0.1, 0.3];
    $filter = fn(float $value): bool => $value < 0.6;
    $simple = new FloatImmutableSequence($this->values);
    $filtered = $simple->filter($filter);
    $this->assertEquals(3, $filtered->count());
    $this->assertSame($expected, $filtered->toArray());
  }

}
