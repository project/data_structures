<?php

declare(strict_types=1);

namespace Drupal\Tests\data_structures\Unit;

use Drupal\data_structures\BaseTypes;
use Drupal\data_structures\Validator\Type;
use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group data_structures
 *
 * @coversDefaultClass \Drupal\data_structures\Validator\Type
 */
final class BaseTypesValidatorTest extends UnitTestCase {

  /**
   * Test base types.
   *
   * @covers ::__invoke
   */
  public function testBaseTypes(): void {
    $validator = new Type();
    foreach (BaseTypes::cases() as $type) {
      $this->assertEquals($type->value, $validator($type->value), 'Valid types should return without exception.');
    }
  }

  /**
   * Test class names.
   *
   * @covers ::__invoke
   */
  public function testClassNames(): void {
    $validator = new Type();
    $this->assertEquals('BaseTypesValidatorTest', $validator('BaseTypesValidatorTest'), 'Valid class names should return without exception');
    $this->assertEquals('Simple', $validator('Simple'), 'Valid class names should return without exception');
  }

  /**
   * Test lowercase string that is not a type.
   *
   * @covers ::__invoke
   */
  public function testNonTypeString() {
    $this->expectException(\UnexpectedValueException::class);
    $validator = new Type();
    $validator('lowercase');
  }

}
